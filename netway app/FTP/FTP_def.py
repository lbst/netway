import ftplib,os

def download():
    
    #Renseignement des variables
    host = input("Host : ")
    user = input("User : ")
    PWD = input("PWD : ")

    path = input("Chemin (racine /): ")
    filename = input("Nom du fichier : ")

    ftp = ftplib.FTP(host) #Initialisation de la connexion
    ftp.login(user,PWD) #Connexion à l'host
    ftp.cwd(path) #Déplacement dans le répertoire
    ftp.retrbinary("RETR " + filename ,open(filename, "wb").write) #Téléchargement du fichier
    ftp.quit() # Fermeture de la connexion
    print("Téléchargement de ",filename,"effectué.")

def upload():
    
    #Renseignement des variables
    host = input('Host : ')
    user = input('User : ')
    PWD = input('PWD : ')

    path = os.chdir(input("Chemin (façon Linux '/' entre les répertoires): "))
    fichier = input('Fichier : ')

    session = ftplib.FTP(host,user,PWD) #Ouverture de la connexion au serveur FTP + session
    file = open(fichier,'rb') #Ouverture du fichier à envoyer
    session.storbinary('STOR '+ fichier, file) #Envoie du fichier
    file.close() #Fermeture du fichier
    session.quit() #Fermeture de la connexion
    print("Envoie du fichier", fichier, "effectué.")

def rename():
    
    #Renseignement des variables
    host = input("Host : ")
    user = input("User : ")
    PWD = input("PWD : ")

    renommer = input('Tapez le nom du fichier / dossier à renommer : ') #Nom du fichier à renommer ou nom du dossier sans les /
    renommer_en = input('Le renommer en : ') #Nom de sortie, vous pouvez aussi changer l'extension

    session = ftplib.FTP(host,user,PWD) #Ouverture de la connexion au serveur FTP + session
    rename = session.rename(renommer, renommer_en) #Renommage du fichier/répertoire
    session.quit() #Fermeture de la connexion

    print("Fichier/Dossier", renommer,"renommé en", renommer_en)

def create_dir():

    #Renseignement des variables
    host = input("Host : ")
    user = input("User : ")
    PWD = input("PWD : ")

    session = ftplib.FTP(host,user,PWD) #Ouverture de la connexion au serveur FTP + session
    rep = input('Tapez le nom du répertoire à créer : ') #Nom du répertoire
    repertoire = session.mkd(rep) #Création du répertoire
    session.quit() #Fermeture de la connexion

    print("Répertoire", rep,"créé.")

def delete_dir():
    
    #Renseignement des variables
    host = input("Host : ")
    user = input("User : ")
    PWD = input("PWD : ")

    session = ftplib.FTP(host,user,PWD) #Ouverture de la connexion au serveur FTP + session
    supprimer = input('Tapez le nom du répertoire à supprimer : ') #Nom du dossier
    delete_dir = session.rmd(supprimer) #Supression du dossier
    session.quit() #Fermeture de la connexion

    print("Répertoire", supprimer,"supprimé.")

def delete_file():
    
    #Renseignement des variables
    host = input("Host : ")
    user = input("User : ")
    PWD = input("PWD : ")

    effacer = input('Tapez le chemin complet du fichier à effacer : ') #Nom du fichier à effacer

    session = ftplib.FTP(host,user,PWD) #Ouverture de la connexion au serveur FTP + session
    delete = session.delete(effacer) #Suppression du fichier
    session.quit() #Fermeture de la connexion

    print("Suppression de ", effacer,"effectué.")

def print_dir():
    
    #Renseignement des variables
    host = input("Host : ")
    user = input("User : ")
    PWD = input("PWD : ")

    directory = input("Entrer le chemin souhaitez (racine /) : ")

    session = ftplib.FTP(host,user,PWD) #Ouverture de la connexion au serveur FTP + session
    print("Voici les répertoires et fichiers présent dans",directory)
    print(session.dir(directory)) #Affichage des dossiers et fichiers
