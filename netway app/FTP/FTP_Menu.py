from FTP_def import download,upload,rename,create_dir,delete_dir,delete_file,print_dir
import ftplib,os,sys


def choix():

    choix = int(input("Quel outil voulez-vous utiliser ? \n1. Download\n2. Upload\n3. Rename\n4. Create directory\n5. Delete Directory\n6. Delete File\n7. Lister les fichiers\n9. Menu précédent\n0. Quitter\n"))

    if choix == 1 :
        download()

    if choix == 2 :
        upload()

    if choix == 3 :
        rename()

    if choix == 4 :
        create_dir()

    if choix == 5 :
        delete_dir()

    if choix == 6 :
        delete_file()

    if choix == 7 :
        print_dir()

    if choix == 9 :
        exec(open("./Menu_AS.py").read())
        
    if choix == 0 :
        sys.exit()

boucle = True

while boucle == True: # Tant que la 'reponse' est nulle
    choix()
    if choix in (1,2,3,4,5,6,7,0):
        boucle = False
